# Application development

 Using Python, Django, Vagrant

#Prerequisites:
    Vagrant
    VirtualBox
    
#Choose the vagrant box from https://app.vagrantup.com/techbytes/boxes/djangoubuntu

Run the powershell:
- Go to the directory of the project

#Run command:
 vagrant init techbytes/djangoubuntu
 vagrant up
 vagrant ssh
     
On VM:
 - cd /vagrant
 - django startproject myproject
     
#Create files with the python code

#models.py
    
    from django.db import models

  class employees(models.Model):
    firstname=models.CharField(max_length=10)
    lastname=models.CharField(max_length=10)
    emp_id=models.IntegerField()
    
    def _str_(self):
        return self.firstname
        
        
#views.py
    
  from django.shortcuts import render
  from django.http import HttpResponse
  from django.shortcuts import get_object_or_404
  from rest_framework.views import APIView
  from rest_framework.response import Response
  from rest_framework import status
  from . models import employees
  from . serializer import employeesSerializers

  class employeeList(APIView):
    
    def get(self, request):
        employees1= employees.objects.all()
        serializer=employeesSerializers(employees1, many=True)
        return Response(serializer.data)
    
    def post(self):
        pass

#urls.py

    from django.conf.urls import url
from django.contrib import admin
from rest_framework.urlpatterns import format_suffix_patterns
from webapp import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^employees/', views.employeeList.as_view()),

]

#Python3 manage.py runserver